<?if ( !defined( 'B_PROLOG_INCLUDED' ) || B_PROLOG_INCLUDED !== true ) die();

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$arComponentParameters = [
    'PARAMETERS' => [
		'PERCENT_AUTHORIZED' => [
			'PARENT' => 'DATA_SOURCE',
			'NAME' => Loc::getMessage('O_PERCENT_AUTHORIZED'),
			'TYPE' => 'STRING',
			'DEFAULT' => '',
		],
		'PERCENT_NOT_AUTHORIZED' => [
			'PARENT' => 'DATA_SOURCE',
			'NAME' => Loc::getMessage('O_PERCENT_NOT_AUTHORIZED'),
			'TYPE' => 'STRING',
			'DEFAULT' => '',
		]
	]
];?>