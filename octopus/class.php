<?if ( !defined( 'B_PROLOG_INCLUDED' ) || B_PROLOG_INCLUDED !== true ) die();

use \Bitrix\Main\Localization\Loc;

class ExchangeRates extends CBitrixComponent
{
    /**
     * @brief Перезаписать параметры
     * @return массив $arParams
     **/
    public function onPrepareComponentParams($arParams)
    {
        $arParams['PERCENT'] = trim($arParams['PERCENT']);

        $arParams['USE_CAPTCHA'] = trim($arParams['USE_CAPTCHA']);
        if (strlen($arParams['USE_CAPTCHA']) == 0)
            $arParams['USE_CAPTCHA'] = 'N';
        
        $arParams['CACHE_FILTER'] = trim($arParams['CACHE_FILTER']);
        if (strlen($arParams['CACHE_FILTER']) <= 0)
        	$arParams['CACHE_FILTER'] = 'N';

        $arParams['CACHE_GROUPS'] = trim($arParams['CACHE_GROUPS']);
        if (strlen($arParams['CACHE_GROUPS']) <= 0)
        	$arParams['CACHE_GROUPS'] = 'N';
        
        $arParams['CACHE_TIME'] = trim($arParams['CACHE_TIME']);
        if (strlen($arParams['CACHE_TIME']) <= 0)
        	$arParams['CACHE_TIME'] = '36000';
        
        $arParams['CACHE_TYPE'] = trim($arParams['CACHE_TYPE']);
        if (strlen($arParams['CACHE_TYPE']) <= 0)
        	$arParams['CACHE_TYPE'] = 'N';

        return $arParams;
    }

    /**
     * @brief Проверим обязательные параметры
     **/
    protected function checkOptions()
    {
        if ($this->arParams['PERCENT_AUTHORIZED'] && !is_numeric($this->arParams['PERCENT_AUTHORIZED']))
            $this->arResult['ERROR'][] = Loc::getMessage('O_INVALID_COMPONENT_PARAMETER', ['#ID#' => 'PERCENT_AUTHORIZED']);

        if ($this->arParams['PERCENT_NOT_AUTHORIZED'] && !is_numeric($this->arParams['PERCENT_NOT_AUTHORIZED']))
            $this->arResult['ERROR'][] = Loc::getMessage('O_INVALID_COMPONENT_PARAMETER', ['#ID#' => 'PERCENT_NOT_AUTHORIZED']);
    }

    /**
     * @brief Основная логика
     **/
    protected function getResult()
    {
        $url = 'https://www.cbr.ru/scripts/XML_daily.asp';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        $content = curl_exec($ch);
        curl_close($ch);

        $xml = @simplexml_load_string($content);

        $arIdExchangeRates = ['R01235', 'R01239'];
        $arResultExchangeRates = [];

        foreach ($xml->Valute as $item) {
            if (in_array($item['ID'], $arIdExchangeRates)) {
                $arResultExchangeRates[strval($item['ID'])] = [
                    'NAME' => strval($item->Name[0]),
                    'VALUE' => str_replace(',', '.', $item->Value)
                ];
            }
        }

        if ($arResultExchangeRates) {
            global $USER; 
            $percent = 0;
            if ($USER->IsAuthorized() && $this->arParams['PERCENT_AUTHORIZED']) {
                $percent = $this->arParams['PERCENT_AUTHORIZED'];
            } else if (!$USER->IsAuthorized() && $this->arParams['PERCENT_NOT_AUTHORIZED']) {
                $percent = $this->arParams['PERCENT_NOT_AUTHORIZED'];
            }
            
            foreach ($arResultExchangeRates as &$arItem) {
                if (intval($percent) > 0) {
                    $arItem['VALUE'] = intval($arItem['VALUE']) + intval($percent);
                } else {
                    $arItem['VALUE'] = intval($arItem['VALUE']);
                }
            }
            unset($arItem);

            $this->arResult['ITEMS'] = $arResultExchangeRates;
        }
    }

    public function executeComponent()
    {
		try{
            $this->includeComponentLang('class.php');
            $this->checkOptions();
            $this->getResult();

            $this->includeComponentTemplate();
		}catch (Exception $e){
			ShowError($e->getMessage());
		}
    }
};