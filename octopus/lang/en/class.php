<?if ( !defined( 'B_PROLOG_INCLUDED' ) || B_PROLOG_INCLUDED !== true ) die();

$MESS['O_INVALID_COMPONENT_PARAMETER'] = 'Error: Invalid component parameter #ID# (should be a number)';
?>