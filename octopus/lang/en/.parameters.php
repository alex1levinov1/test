<?if ( !defined( 'B_PROLOG_INCLUDED' ) || B_PROLOG_INCLUDED !== true ) die();

$MESS['O_PERCENT_AUTHORIZED'] = 'Percentage for an authorized user';
$MESS['O_PERCENT_NOT_AUTHORIZED'] = 'Percent for unauthorized user';
?>