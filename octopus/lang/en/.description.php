<?if ( !defined( 'B_PROLOG_INCLUDED' ) || B_PROLOG_INCLUDED !== true ) die();

$MESS['O_NAME'] = 'Exchange Rates';
$MESS['O_DESCRIPTION'] = 'The component requests the current USD and EUR exchange rates';
$MESS['OCTOPUS_PATH_ADDITIONAL'] = 'Octopussy';
$MESS['O_PATH_ADDITIONAL'] = 'Exchange Rates';
?>