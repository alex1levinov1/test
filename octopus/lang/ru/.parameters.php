<?if ( !defined( 'B_PROLOG_INCLUDED' ) || B_PROLOG_INCLUDED !== true ) die();

$MESS['O_PERCENT_AUTHORIZED'] = 'Процент для авторизованного пользователя';
$MESS['O_PERCENT_NOT_AUTHORIZED'] = 'Процент для неавторизованного пользователя';
?>