<?if ( !defined( 'B_PROLOG_INCLUDED' ) || B_PROLOG_INCLUDED !== true ) die();

$MESS['O_INVALID_COMPONENT_PARAMETER'] = 'Ошибка: неправильный параметр компонента #ID# (должно быть числом)';
?>