<?if ( !defined( 'B_PROLOG_INCLUDED' ) || B_PROLOG_INCLUDED !== true ) die();

$MESS['O_NAME'] = 'Курс валют';
$MESS['O_DESCRIPTION'] = 'Компонент запрашивает актуальный курс валют USD и EUR';
$MESS['OCTOPUS_PATH_ADDITIONAL'] = 'Осьминожка';
$MESS['O_PATH_ADDITIONAL'] = 'Курс валют';
?>