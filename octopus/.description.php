<?if ( !defined( 'B_PROLOG_INCLUDED' ) || B_PROLOG_INCLUDED !== true ) die();

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
	'NAME' => Loc::getMessage('O_NAME'),
	'DESCRIPTION' => Loc::getMessage('O_DESCRIPTION'),
	'ICON' => '/images/sale_account.gif',
	'SORT' => 100,
	'PATH' => array(
		'ID'   => 'OCTOPUS',
		'NAME' => Loc::getMessage('OCTOPUS_PATH_ADDITIONAL'),
		'CHILD' => array(
			'ID' => 'octopus',
			'NAME' => Loc::getMessage('O_PATH_ADDITIONAL'),
			'SORT' => 100,
   			'CHILD' => array(
				'ID' => 'octopus_cmpx',
			),
		),
	)
);?>