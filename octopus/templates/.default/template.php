<?if ( !defined( 'B_PROLOG_INCLUDED' ) || B_PROLOG_INCLUDED !== true ) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if ($arResult['ITEMS']) {?>
    <div class="exchange-rates container">
        <?if ($arResult['ERROR'] && count($arResult['ERROR']) > 0) {?>
            <ul class="exchange-rates__error">
                <?foreach ($arResult['ERROR'] as $error) {?>
                    <li><?=$error?></li>
                <?}?>
            </ul>
        <?}?>
    
        <h2 class="exchange-rates__title"><?=GetMessage('EXCHANGE_RATES_TITLE')?></h2>
        <ul class="exchange-rates__list">
            <?if ($arResult['ITEMS']['R01235']['VALUE']) {?>
                <li class="exchange-rates__item">
                    <img class="exchange-rates__img" src="<?=$templateFolder?>/images/USD.png" alt="USD">
                    <p class="exchange-rates__name">USD - </p>
                    <p class="exchange-rates__value"><?=$arResult['ITEMS']['R01235']['VALUE']?>₽</p>
                </li>
            <?}?>
            <?if ($arResult['ITEMS']['R01239']['VALUE']) {?>
                <li class="exchange-rates__item">
                    <img class="exchange-rates__img" src="<?=$templateFolder?>/images/EUR.png" alt="EUR">
                    <p class="exchange-rates__name">EUR - </p>
                    <p class="exchange-rates__value"><?=$arResult['ITEMS']['R01239']['VALUE']?>₽</p>
                </li>
            <?}?>
        </ul>
    </div>
<?}?>